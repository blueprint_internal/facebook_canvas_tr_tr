
function CanvasBuildout(resources)
{
	CanvasBuildout.resources = resources;
}
CanvasBuildout.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(900, 768, Phaser.CANVAS, 'CanvasBuildout', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		this.game.scale.maxWidth = 900;
    	this.game.scale.maxHeight = 768;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone', CanvasBuildout.resources.phone);
		this.game.load.image('img_1', CanvasBuildout.resources.photo_1);
		this.game.load.image('img_2', CanvasBuildout.resources.photo_2);
		this.game.load.image('img_3', CanvasBuildout.resources.photo_3);
		this.game.load.image('img_4', CanvasBuildout.resources.photo_4);
		this.game.load.image('button', CanvasBuildout.resources.button);
		this.game.created = false;
    	/*
    	
    	this.game.stage.backgroundColor = '#ffffff'
    	*/
    	
	},

	create: function(evt)
	{
		if(this.game.created === false)
		{
			this.game.created = true;
			this.game.stage.backgroundColor = '#ffffff'
			this.parent.phone = this.game.add.sprite(5,0,'phone');
			this.parent.img_1 = this.game.add.sprite(600,0,'img_1');
			this.parent.img_1.origin = {x:700,y:0};
			this.parent.img_2 = this.game.add.sprite(450,205,'img_2');
			this.parent.img_2.origin = {x:750,y:205};
			this.parent.img_3 = this.game.add.sprite(520,380,'img_3');
			this.parent.img_3.origin = {x:720,y:380};
			this.parent.img_4 = this.game.add.sprite(470,550,'img_4');
			this.parent.img_4.origin = {x:770,y:610};
			//
			this.parent.button = this.game.add.sprite(490,540,'button');
			this.parent.button.origin = {x:690,y:540};
			this.parent.button.anchor.set(0.5);
			var style = { font: "bold 19px freight-sans-pro", fill: "#ffffff", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
			var style2 = { font: "bold 22px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:170, align: "center",lineSpacing: -10 };
			this.parent.buttonText = this.parent.button.addChild(this.game.make.text(0,4, CanvasBuildout.resources.button_text, style));
			this.parent.boxText = this.game.make.text(0,5, CanvasBuildout.resources.box_text,style2);
			this.parent.buttonText.anchor.set(0.5);
			this.parent.button.smoothed =false;
			this.parent.boxText.fill = "#000000";
			
			var textBackground = this.game.make.graphics(0,0);
			this.parent.textBoxGroup =  this.game.add.group();
			this.parent.textBoxGroup.add(textBackground);
			this.parent.textBoxGroup.add(this.parent.boxText);
			this.parent.textBoxGroup.x = 420;
			this.parent.textBoxGroup.y = 150;
			this.parent.textBoxGroup.origin = {x:420,y:150};
			
			textBackground.beginFill(0xebebeb, 1);
	    	textBackground.drawRect(0,0, this.parent.boxText.width+10, this.parent.boxText.height+10);
	    	
	    	this.game.time.events.add(3000, this.parent.buildAnimation,this.parent);
	    }
	},
    
	buildAnimation: function()
	{
		var phoneX = this.phone.x+this.phone.width/2;
    	var phoneY = this.phone.height/2;
    	this.animations = [];
    	this.scales = [];
    	this.fades =[]
		for(var i = 0;i<4;i++)
    	{
    		var cnt = i+1;
    		var subj = this['img_'+cnt];

    		subj.x = this['img_'+cnt].origin.x;
    		subj.y = this['img_'+cnt].origin.y;
    		subj.alpha = 1;
    		subj.anchor.set(0.5,0.5);
    		subj.scale.x = 1;
    		subj.scale.y = 1;
    		var tw = this.game.add.tween(subj);
    		var tf = this.game.add.tween(subj);
    		
    		tw.to({x: phoneX,y: phoneY},800,Phaser.Easing.Quadratic.Out);
    		var tt = this.game.add.tween(subj.scale);
    		tt.to({x: .5, y: .5},300,Phaser.Easing.Quadratic.Out);
			tf.to( { alpha:0},300,Phaser.Easing.Quadratic.Out);
    		this.animations.push(tw);
    		this.scales.push(tt);
    		this.fades.push(tf);
    		this.animations[i].chain(tt,tf);

    	}
    	this.but_anim = this.game.add.tween(this.button).to({x: phoneX,y: phoneY},800,Phaser.Easing.Quadratic.Out);
    	this.but_anim.to( { alpha:0},200,Phaser.Easing.Quadratic.Out);
    	this.boxText_anim = this.game.add.tween(this.textBoxGroup).to({x: phoneX,y: phoneY},800,Phaser.Easing.Quadratic.Out);
    	this.boxText_anim.to( { alpha:0},200,Phaser.Easing.Quadratic.Out);
		this.animations[0].chain(this.scales[0],this.fades[0],this.animations[1],this.scales[1],this.fades[1],this.but_anim,this.animations[2],this.scales[2],this.fades[2],this.boxText_anim,this.animations[3],this.scales[3],this.fades[3]);
		this.animations[0].start();
    	
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


